package com.v24.backend.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Autotest {
	
	@GetMapping("/autotest")
	public ResponseEntity<?> autotest() {
		String jsonResponse = "{\"message\": \"---------------------------- ok autotest ----------------------------\"}";		
        return ResponseEntity.ok(jsonResponse);
	}
}
